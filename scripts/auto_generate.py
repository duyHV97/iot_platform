import paho.mqtt.client as mqttClient
import time
from datetime import datetime
import random
import json
connected = False

brokerUri = 'localhost'
topic = 'iot_platform'
Connected = False


def generateData():

    data2 = {
        "gateway_id": "A",
        "device_id": 2,
        "temperature": random.randint(11, 50),
        "humidity": random.randint(40, 100),
        "CO2": random.randint(300, 1000),
        "pH": random.randint(4, 14),
        "longitude": 20.9979077,
        "latitude": 105.7885501,
        "time": datetime.now().strftime("%m-%d-%Y %H:%M:%S"),
        "timestamp": round(datetime.timestamp(datetime.now()))

    }

    data3 = {
        "gateway_id": "A",
        "device_id": 3,
        "temperature": random.randint(11, 50),
        "humidity": random.randint(40, 100),
        "CO2": random.randint(300, 1000),
        "pH": random.randint(4, 14),
        "longitude": 20.9911442,
        "latitude": 105.8078286,
        "time": datetime.now().strftime("%m-%d-%Y %H:%M:%S"),
        "timestamp": round(datetime.timestamp(datetime.now()))
    }

    data4 = {
        "gateway_id": "A",
        "device_id": 4,
        "temperature": random.randint(11, 50),
        "humidity": random.randint(40, 100),
        "CO2": random.randint(300, 1000),
        "pH": random.randint(4, 14),
        "longitude": 21.009847,
        "latitude": 105.829132,
        "time": datetime.now().strftime("%m-%d-%Y %H:%M:%S"),
        "timestamp": round(datetime.timestamp(datetime.now()))

    }

    data5 = {
        "gateway_id": "A",
        "device_id": 5,
        "temperature": random.randint(11, 50),
        "humidity": random.randint(40, 100),
        "CO2": random.randint(300, 1000),
        "pH": random.randint(4, 14),
        "longitude": 21.009849,
        "latitude": 105.829138,
        "time": datetime.now().strftime("%m-%d-%Y %H:%M:%S"),
        "timestamp": round(datetime.timestamp(datetime.now()))
    }
    print('Auto generate: %s, type: %s' % (data2, type(data2)))
    print('Auto generate: %s, type: %s' % (data3, type(data3)))
    print('Auto generate: %s, type: %s' % (data4, type(data4)))

    return [json.dumps(data3), json.dumps(data4), json.dumps(data2), json.dumps(data5)]

def run():
    client = mqttClient.Client('python')
    client.connect(host=brokerUri, port=1883)
    i = 0
    while True:
        if i < 20:
            value = generateData()
            for j in value:
                # print(type(json.dumps(j)))
                client.publish(topic, j, qos=1)
                time.sleep(1)
            i += 1

        i = 0
        client.disconnect()
        client.reconnect()

run()
