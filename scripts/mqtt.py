import time
import sys
import paho.mqtt.client as mqtt
import json
import random
 
THINGSBOARD_HOST = '113.178.64.41'
 
ACCESS_TOKEN = 'wifi01'
 
sensor_data = {'id':"wifi01",'temperature': 0,'humidity':0}
 
minA = 10 
maxA = 70
 
client = mqtt.Client()
 
 
client.username_pw_set(ACCESS_TOKEN)
 
client.connect(THINGSBOARD_HOST, 1883)
 
client.loop_start()
 
try:
	while True:
		temperature = random.randrange(minA,maxA)
		humidity = random.randrange(minA,maxA)

		print("temperature:{:g}".format(temperature))
		print("humidity:{:g}".format(humidity))

		sensor_data['temperature'] = temperature
		sensor_data['humidity'] = humidity

		client.publish('v1/devices/me/telemetry',json.dumps(sensor_data))
		time.sleep(1)
except KeyboardInterrupt:
    pass
client.loop_stop()
client.disconnect()