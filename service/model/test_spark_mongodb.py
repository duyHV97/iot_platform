
import findspark
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from service.model.ml_utils import MlUtils
from tensorflow.keras.models import load_model
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
# from sparkdl import KerasTransformer

# findspark.init("3rd-parties/spark-3.0.0-preview2-bin-hadoop2.7")
matplotlib.use('TKAgg')
my_spark = SparkSession \
    .builder \
    .appName("myApp") \
    .config("spark.mongodb.input.uri", "mongodb://127.0.0.1/iot_platform.modeling_2020-06-10")\
    .config("spark.mongodb.output.uri", "mongodb://127.0.0.1/iot_platform.modeling_2020-06-10")\
    .config('spark.sql.execution.arrow.enabled', 'true')\
    .getOrCreate()

df = my_spark.read.format("com.mongodb.spark.sql.DefaultSource").load()
# df.show()
# print(df.count())

# kafka_df = my_spark.readStream.format('kafka')\
#     .option('kafka.bootstrap.servers', 'localhost:9092')\
#     .option('subscribe', 'iot_platform').load()

temperature = df.select('timestamp', 'temperature', ).filter(df.device_id == 2)
temperature.show()
temperature = temperature.toPandas()
temperature = temperature.drop(index=0).iloc[:100]
temperature = temperature.set_index('timestamp')
ml_utils = MlUtils(temperature)
print(ml_utils.min_max_scale()[:6])
model_path = 'service/model/simple_predictive_RNN.h5'
# transformer = KerasTransformer(inputCol="temperature",
#                                outputCol="predictions",
#                                modelFile=model_path)
model = load_model(model_path)
print(temperature.shape)
predicts = model.predict(ml_utils.min_max_scale()[:100].reshape((1, len(temperature), 1)))
real_predicts = ml_utils.invert_transform(predicts)

print(real_predicts[0])

plt.plot(real_predicts[0])
plt.show()
# final_df = transformer.transform((temperature))
# final_df.show()
# 3rd_parties/spark-2.4.5-bin-hadoop2.7/bin/spark-submit --packages org.mongodb.spark:mongo-spark-connector_2.11:2.3.1 service/model/test_spark_mongodb.py
# 3rd_parties/spark-2.4.5-bin-hadoop2.7/bin/spark-submit --packages org.mongodb.spark:mongo-spark-connector_2.11:2.3.1,org.apache.spark:spark-sql-kafka-0-10_2.12:2.4.5 service/spark_jobs/test_spark_mongodb.py
