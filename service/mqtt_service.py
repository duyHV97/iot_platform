import json
from time import sleep

from paho.mqtt import MQTTException
from paho.mqtt.client import Client

from service.kafka_service import KafkaService
from utils.common import Common, Logger


class MQTTService(object):
    def __init__(self, client_id, log_object):
        self.mqtt_config = Common.load_config(
            path='./config/mqtt.yaml')['mqtt']
        self.__host = self.mqtt_config['host']
        self.__port = self.mqtt_config['port']
        self.__topic = self.mqtt_config['topic']
        self.qos = int(self.mqtt_config['qos'])
        self.mqtt_client = Client(client_id=client_id, clean_session=False)
        self.logger = log_object
        self.mqtt_topics = []

        self.set_callbacks()

        self.kafka_service = KafkaService(log_obj=log_object)

    def connect(self, keepalive=60, bind_address='', reconnect=True, retry_freq=2):
        is_connected = False

        try:
            self.mqtt_client.connect(host=self.__host,
                                     port=self.__port,
                                     keepalive=keepalive,
                                     bind_address=bind_address)
            self.logger.info('MQTT: Connected to %s:%s' %
                             (self.__host, self.__port))

        except ConnectionError as e:
            self.logger.error(
                'MQTT: Cannot connect to %s:%s [ERROR: %s]' % (
                    self.__host,
                    self.__port,
                    e
                )
            )

            if reconnect:

                while not is_connected:

                    try:
                        self.mqtt_client.reconnect()

                    except ConnectionError as error:
                        self.logger.error(
                            'MQTT: Cannot reconnect [ERROR: %s]' % e)

                else:
                    is_connected = True

    def publish(self, msg):

        try:
            self.logger.info(
                'MQTT: Published [MESSAGE: %s] -> [TOPIC: %s]' % (msg, self.__topic))
            self.mqtt_client.publish(
                topic=self.__topic, payload=msg, qos=self.qos)
            sleep(1.0)

        except Exception as e:
            self.logger.error(
                'MQTT: Cannot published [MESSAGE: %s] -> [TOPIC: %s] [ERROR: %s]' % (
                    str(msg),
                    self.__topic,
                    e
                )
            )

    def subscribe(self):

        try:
            self.logger.info(
                'MQTT: Subscribed -> [TOPIC: %s]' % (self.__topic))
            self.mqtt_client.subscribe(topic=self.__topic, qos=self.qos)

        except Exception as e:
            self.logger.error(
                'MQTT: Cannot subscribe -> [TOPIC: %s] [ERROR: %s]' % (
                    self.__topic,
                    e
                )
            )

    def on_connect(self, client, userdata, flags, rc):
        pass

    def on_subscribe(self, client, obj, mid, granted_qos):
        self.logger.info('MQTT: Subscribed: %s: \tgranted_qos: %s' %
                         (str(mid), str(granted_qos)))

    def on_publish(self, client, obj, mid):
        self.logger.info('MQTT: mid: %s' % str(mid))

    def on_message(self, client, obj, message):

        if message is not None:

            try:
                str_message = message.payload.decode('utf-8')
                self.logger.info(
                    'MQTT: [MESSAGE: %s] from [TOPIC: %s] [CLIENT: %s]' % (
                        str_message,
                        message.topic,
                        client
                    )
                )
                self.kafka_service.produce_message(str_message)
                self.logger.info(
                    'MQTT: [MESSAGE: %s] -> kafka [TOPIC: %s]' % (
                        str_message,
                        self.kafka_service.topic
                    )
                )

            except Exception as e:
                self.logger.error('MQTT: [ERROR: %s]' % e)

    def set_callbacks(self):
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.mqtt_client.on_subscribe = self.on_subscribe
        self.mqtt_client.on_publish = self.on_publish
