import json
import random
from datetime import datetime

import pymongo

from utils.common import Common, Logger


class MongodbService(object):
    def __init__(self, log_object):
        self.logger = log_object
        self.mongo_configs = Common.load_config('config/mongo.yaml')['mongo']
        self.__host = self.mongo_configs['host']
        self.__port = self.mongo_configs['port']

        self.__client = pymongo.MongoClient(host=self.__host, port=self.__port)
        self.__database = self.__client[self.mongo_configs['database']]
        self.__collection = self.__database[self.mongo_configs['collection']]

    def insert(self, record, collection):

        try:
            self.logger.info(
                'MONGO: insert [RECORD: %s] -> [COLLECTION: %s]' % (str(record),
                                                                    str(self.__collection))
            )
            collection.insert_one(json.loads(record))

        except Exception as e:
            self.logger.error(
                'MONGO: cannot insert [MESSAGE: %s, ERROR: %s]' % (str(record), e))

    def insert_all(self, list_records, collection):

        try:
            collection.insert_many(list_records)
            self.logger.info("MONGO: insert into [COLLECTION: %s]",
                             collection)

        except Exception as e:
            self.logger.info("MONGO: cannot insert to [COLLECTION: %s] with error [ERRPR: %s]",
                             str(collection),
                             e)
            raise e

    def get_collection(self, database, collection_name):

        return self.__client[database][collection_name]

    def get_all(self, collection_name):
        messages = self.__database[collection_name].find({})

        return messages, messages.count(True)

    def get_with_limitation(self, collection_name, limitation):
        messages = self.__database[collection_name].find({}).limit(limitation)

        return messages

    def is_existed_device_id(self, device_id, collection_name):
        result_cursor = self.__database[collection_name].find({'device_id': device_id})

        if result_cursor.count() == 0:
            return True

        return False

    def find_hour_records_with_limitation(self, collection_name, device_id, limitation):

        result_cursor = self.__database[collection_name].find({'device_id': device_id}).where(
            'this.timestamp % 900 == 0'
        ).limit(limitation)

        return result_cursor

    def find_hour_records(self, collection_name, device_id):

        result_cursor = self.__database[collection_name].find({'device_id': device_id}).where(
            'this.timestamp % 900 == 0'
        )

        return result_cursor

    def find_all_message_in_3_days(self, collection_name, device_id):
        """
        1 minute = 1 record, => 3 days = 4320 records
        """
        result = self.__database[collection_name].find({'device_id': device_id}).where(
            'this.timestamp % 60 == 0'
        ).sort('timestamp', -1).limit(4320)

        return result

    def find_per_1_minute_msg_1day(self, collection_name, device_id):
        """
        1440 = 1*60*24 (the 1 lastest day)

        Args:
            collection_name (str): collection name
            device_id (int): device id
        """
        results = self.__database[collection_name].find({'device_id': device_id}).where(
            'this.timestamp % 60 == 0'
        ).sort('timestamp', -1).limit(1440)

        return reversed(list(results))


    def drop_collection(self, collection_name):
        self.logger.info('Drop collection: %s', collection_name)
        self.__database.drop_collection(collection_name)

    def get_all_collection_names(self):
        return self.__database.list_collection_names()

    def get_all_device_id(self):
        device_ids = []
        devices, _ = self.get_all('device')
        for device in devices:
            device_ids.append(device['device_id'])

        return device_ids

if __name__ == "__main__":
    mongo = MongodbService(Logger(__name__).get_logger())
    collection_names = mongo.get_all_collection_names()
    for collection_name in collection_names:
        if collection_name.startswith('create_model_device'):
            mongo.drop_collection(collection_name=collection_name)
