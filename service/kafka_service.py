import ast
import json
import sys

import requests
from confluent_kafka import Consumer, KafkaError, KafkaException, Producer
from confluent_kafka.admin import AdminClient, NewTopic

from service.mongodb_service import MongodbService
from utils.common import Common, Logger


class KafkaService(object):
    def __init__(self, log_obj):
        self.kafka_configs = Common.load_config(
            path='./config/kafka.yaml')['kafka']
        self.__bootstrap_server = self.kafka_configs['bootstrap.servers']
        self.__topic = self.kafka_configs['topic']
        self.__group_id = self.kafka_configs['group.id']
        self.logger = log_obj
        self.__admin_conf = {
            'bootstrap.servers': self.__bootstrap_server
        }
        self.__admin = AdminClient(conf=self.__admin_conf)

    def create_topics(self, topic_name_list):
        new_topics = [
            NewTopic(
                topic_name,
                num_partitions=1,
                replication_factor=1
            ) for topic_name in topic_name_list
        ]
        fs = self.__admin.create_topics(new_topics=new_topics)

        for topic, f in fs.items():

            try:
                f.result()
                self.logger.info('KAFKA: Created [TOPIC: %s]' % topic)

            except Exception as e:
                self.logger.error(
                    'KAFKA: Cannot create [TOPIC: %s], [ERROR: %s]' % (topic, e))

    def delete_topics(self, topic_name_list):

        try:
            self.__admin.delete_topics(topics=topic_name_list)
            self.logger.info('KAFKA: Deleted [TOPIC: %s]' % topic_name_list)

        except Exception as e:
            self.logger.error(
                'KAFKA: Cannot delete [TOPIC: %s] [ERROR: %s]' % (topic_name_list, e))

    def read_message(self, topic_name_list):

        conf = {
            'bootstrap.servers': self.__bootstrap_server,
            'group.id': 'iot_platform',
            'default.topic.config': {
                'auto.offset.reset': 'largest'
            }
        }
        consumer = Consumer(conf)
        consumer.subscribe(topics=topic_name_list)

        try:

            while True:
                msg = consumer.poll(1.0)

                if msg is None:
                    continue

                if msg.error():

                    if msg.error().code() == KafkaError._PARTITION_EOF:
                        continue
                    else:
                        self.logger.err(msg.error())
                        break
                self.logger.info(
                    'KAFKA: Received [MESSAGE: %s]' % msg.value().decode('utf-8'))

        except Exception as e:
            self.logger.error('KAFKA: Cannot read messages! [ERROR: %s]' % e)

        finally:
            consumer.close()

    def read_and_insert_to_mongo(self, topic_name_list):
        mongo_service = MongodbService(self.logger)
        conf = {
            'bootstrap.servers': self.__bootstrap_server,
            'group.id': 'iot_platform',
            'default.topic.config': {
                'auto.offset.reset': 'largest'
            }
        }
        consumer = Consumer(conf)
        consumer.subscribe(topics=topic_name_list)

        try:

            while True:
                msg = consumer.poll(1.0)

                if msg is None:
                    continue

                if msg.error():

                    if msg.error().code() == KafkaError._PARTITION_EOF:
                        continue

                    else:
                        self.logger.err(msg.error())
                        break

                str_msg = msg.value().decode('utf-8')
                self.logger.info('KAFKA: Received [MESSAGE: %s]' % str_msg)
                json_msg = json.loads(str_msg)
                devices, number_of_device = mongo_service.get_all("device")
                collection1 = mongo_service.get_collection(
                    'iot_platform', 'collection1')
                device_collection = mongo_service.get_collection(
                    'iot_platform', 'device')
                # json_msg2 = json.loads(json_msg)

                if number_of_device == 0:
                    new_device = {
                        "gateway_id": json_msg["gateway_id"],
                        "device_id": int(json_msg["device_id"]),
                        "longitude": float(json_msg["longitude"]),
                        "latitude": float(json_msg["latitude"])
                    }

                    mongo_service.insert(record=json.dumps(new_device),
                                         collection=device_collection)

                for device in devices:
                    if mongo_service.is_existed_device_id(json_msg['device_id'], 'device'):
                        new_device = {
                            "gateway_id": json_msg["gateway_id"],
                            "device_id": int(json_msg["device_id"]),
                            "longitude": float(json_msg["longitude"]),
                            "latitude": float(json_msg["latitude"])
                        }
                        mongo_service.insert(json.dumps(
                            new_device), device_collection)

                mongo_service.insert(json.dumps(json_msg), collection1)

        except Exception as e:
            self.logger.error('KAFKA: Cannot read messages! [ERROR: %s]' % e)

        finally:
            consumer.close()

    def produce_message(self, message):

        def delivery_callback(err, msg):
            """
            Called once for each message produced to indicate delivery result.
            Triggered by poll() or flush().
            """

            if err is not None:
                self.logger.error(
                    'KAFKA: Message delivery failed: {}'.format(err))

            else:
                self.logger.info(
                    'KAFKA: Produce [MESSAGE: %s] -> [TOPIC: %s] [PARTITION: %s]' % (
                        msg,
                        msg.topic(),
                        msg.partition()
                    )
                )

        producer = Producer(**self.__admin_conf)
        # producer.poll(0)

        try:
            producer.produce(
                self.__topic,
                message.encode('utf-8'),
                callback=delivery_callback
            )
            producer.poll(0)

        except Exception as e:
            self.logger.error(
                'KAFKA: Cannot publish [MESSAGE: %s] -> [TOPIC: %s] [ERROR: %s]' % (
                    message,
                    self.__topic,
                    e
                )
            )
        # Wait for any outstanding messages to be delivered and delivery report
        # callbacks to be triggered.
        producer.flush()

    @property
    def topic(self):
        return self.__topic
