import logging
import os
import sys

import yaml
from pathlib import Path
from datetime import datetime


class Common(object):
    @staticmethod
    def create_dir():
        Path('content/logs/').mkdir(parents=True, exist_ok=True)
        Path('content/luigi_target/').mkdir(parents=True, exist_ok=True)

    @staticmethod
    def load_config(path):
        with open(file=path, mode='r') as config_file:
            config = yaml.load(stream=config_file, Loader=yaml.FullLoader)

        return config

    @staticmethod
    def get_dir(working_dir):

        return os.path.dirname(os.path.realpath(working_dir))

    @staticmethod
    def get_file_name(file_path):
        path = Path(file_path)

        return path.stem

    @staticmethod
    def get_datetime(date):

        return datetime.strftime(date, "%Y-%m-%d")


class Logger(object):
    def __init__(self, logger_name):
        self.config = Common.load_config('./config/log.yaml')
        self.logger_path = self.config['file_path']
        self.logger_name = logger_name

    def logging_config(self):

        if not os.path.exists(path=self.logger_path):
            os.mkdir(path=self.logger_path)
        # get logger
        logger = logging.getLogger(self.logger_name)
        logger.setLevel(logging.INFO)
        # config logger with basic config
        logging.basicConfig(filename=self.logger_path + self.logger_name + '.log',
                            filemode='a',
                            format='%(asctime)s - %(name)s - %(levelname)5s - %(module)s.%(funcName)s - %(message)s',
                            datefmt='%H:%M:%S')
        # console logging
        console_handler = logging.StreamHandler(stream=sys.stdout)
        console_formatter = logging.Formatter(
            '%(asctime)s - %(name)s - %(levelname)5s - %(module)s.%(funcName)s - %(message)s'
        )
        console_handler.setFormatter(console_formatter)
        console_handler.setLevel(logging.DEBUG)
        # add handler
        logger.addHandler(console_handler)

        return logger

    def get_logger(self):
        return self.logging_config()
