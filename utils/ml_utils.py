import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, Normalizer
from tensorflow.keras.preprocessing.sequence import TimeseriesGenerator


class MlUtils(object):

    def timeseries_generator(self, length, batch_size):

        return TimeseriesGenerator(data=self.dataset,
                                   targets=self.dataset,
                                   length=length,
                                   batch_size=batch_size)

    def create_timeseries_dataset(self, X, y, time_steps=1):
        Xs, ys = [], []

        for i in range(len(X) - time_steps):
            v = X.iloc[i: (i + time_steps)].values
            Xs.append(v)
            ys.append(y.iloc[i + time_steps])

        return np.array(Xs), np.array(ys)

    def transform_dataset(self, dataset, timestep):
        Xs = []

        for i in range(len(dataset) - timestep):
            Xs.append(dataset.iloc[i: i + timestep].values)

        return np.array(Xs)
