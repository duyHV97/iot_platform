#!/usr/bin/env bash
# shellcheck disable=SC2164
# shellcheck disable=SC2046

sudo kill -l $(sudo lsof -t -i :2181)

# cd iot_platform/3rd_parties/kafka_2.12-2.4.1
cd 3rd_parties/kafka_2.13-2.5.0/
bin/zookeeper-server-start.sh -daemon config/zookeeper.properties

# cd iot_platform/3rd_parties/kafka_2.12-2.4.1
pwd
bin/kafka-server-start.sh -daemon config/server.properties
pwd
cd ../../

## start consuming
#cd ~/PycharmProjects/iot_platform/kafka_2.12-2.4.1/
# bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic iot_platform --from-beginning
## publish message
# bin/kafka-console-producer.sh --broker-list localhost:9092 --topic iot_platform


# run bash
# /bin/bash  /home/duy/vscode/iot_platform/bin/run.sh
export PYTHONPATH=/home/duy/vscode/iot_platform/

pwd
source /home/duy/vscode/iot_platform/venv/bin/activate

python bin/mqtt_consumer.py &

python bin/kafka_consumer.py
# luigi --module task.predict_RNN ExecutePredictRNN
# luigi --module task.create_model ExecuteCreateModel