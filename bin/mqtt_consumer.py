from service.mqtt_service import MQTTService
from utils.common import Logger


service = MQTTService('mqtt_consumer', Logger('mqtt_consumer').get_logger())
service.connect()
service.subscribe()
service.mqtt_client.loop_forever()
