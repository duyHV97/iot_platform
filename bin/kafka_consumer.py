from service.kafka_service import KafkaService
from utils.common import Logger


service = KafkaService(Logger('kafka_consumer').get_logger())
service.read_and_insert_to_mongo(['iot_platform'])
