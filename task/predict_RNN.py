import luigi
from datetime import datetime
from logics.predict_RNN import PredictRNN
from utils.common import Common, Logger
from pathlib import Path
from service.mongodb_service import MongodbService


logger = Logger(
    Common.get_file_name(__file__) + datetime.strftime(datetime.now(), '%Y%m%d')
).get_logger()

target_dir = Common.load_config(
    'config/predict_RNN.yaml')['config']['target_dir']


class BaseTask(luigi.Task):

    date = luigi.DateParameter(default=datetime.today())

    def requires(self):
        return

    def execute(self):
        pass

    def run(self):
        class_name = self.__class__.__name__
        with self.output().open('w') as fp:
            pass
        logger.info('START %s', class_name)
        self.execute()
        logger.info('END %s', class_name)

    def output(self):
        class_name = self.__class__.__name__
        return luigi.LocalTarget(target_dir + '{}_{}.txt'.format(class_name,
                                                             Common.get_datetime(self.date)))


class WaitTask(luigi.ExternalTask):
    target_name = luigi.Parameter()  # local targert txt file

    def requires(self):
        return luigi.LocalTarget(target_dir + self.target_name)

    def complete(self):
        return True


class CreateInputTable(BaseTask):
    device_id = luigi.Parameter()

    def requires(self):
        wait_target = '{}_{}'.format(
            'DropCollections', Common.get_datetime(self.date))
        return WaitTask(target_name=wait_target)

    def execute(self):
        obj = PredictRNN(logger, self.date)
        obj.create_input_table(device_id=self.device_id)


class PredictTemperature(BaseTask):
    device_id = luigi.Parameter()

    def requires(self):
        obj = PredictRNN(logger=logger, date=self.date)

        for device_id in obj.get_all_device_id():
            yield CreateInputTable(device_id=device_id)

    def execute(self):
        obj = PredictRNN(logger=logger, date=self.date)
        obj.predict_temperature(self.device_id)


class ExecutePredictRNN(luigi.WrapperTask):

    def requires(self):
        mongo = MongodbService(logger)

        for device_id in mongo.get_all_device_id():
            yield PredictTemperature(device_id=device_id)


if __name__ == "__main__":
    print(target_dir)
    files = Path(target_dir).glob('*.txt')
    # print([f for f in files])
    # names = [f.stem for f in files]
    files = [f for f in files]
    # d = dict(zip(names, files))
    # print(names)
    print(files)
