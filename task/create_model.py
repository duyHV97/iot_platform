from datetime import datetime
from pathlib import Path

import luigi

from utils.common import Common, Logger
from logics.create_model import CreateModel

logger = Logger(Common.get_file_name(__file__)).get_logger()
target_path = Common.load_config('config/create_model.yaml')['config']['target_dir']


class BaseTask(luigi.Task):
    date = luigi.DateParameter(default=datetime.today())

    def execute(self):
        pass

    def run(self):
        class_name = self.__class__.__name__
        output_path = target_path.format(class_name,
                                         Common.get_datetime(date=self.date))
        with self.output().open('w') as fp:
            pass

        logger.info('Start %s', class_name)
        Common.create_dir()
        self.execute()
        logger.info('End %s', class_name)

    def output(self):
        class_name = self.__class__.__name__
        output_path = target_path.format(class_name,
                                         Common.get_datetime(date=self.date))

        return luigi.LocalTarget(path=output_path)

    def requires(self):

        return []

class CreateDataSet(BaseTask):
    device_id = luigi.Parameter()

    def requires(self):
        return []

    def execute(self):
        obj = CreateModel(logger=logger, date=self.date)
        obj.create_data_table(device_id=self.device_id)

class ParallelCreateDataset(BaseTask):

    def requires(self):
        obj = CreateModel(logger=logger, date=self.date)

        for device_id in obj.get_all_device_id():
            yield CreateDataSet(device_id=device_id)


class TrainModel(BaseTask):
    device_id = luigi.Parameter()

    def requires(self):
        return ParallelCreateDataset()

    def execute(self):
        obj = CreateModel(logger=logger, date=self.date)
        obj.train_1_feature(device_id=self.device_id)

class ParallelTrainModel(BaseTask):

    def requires(self):
        obj = CreateModel(logger=logger, date=self.date)

        for device_id in obj.get_all_device_id():
            yield TrainModel(device_id=device_id)

class DropCollections(BaseTask):

    def requires(self):
        return ParallelTrainModel()

    def execute(self):
        obj = CreateModel(logger, self.date)
        obj.drop_collections()

class ExecuteCreateModel(luigi.WrapperTask):

    def requires(self):
        return DropCollections()
