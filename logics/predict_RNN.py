import luigi
import pandas as pd
import numpy as np
import json
from datetime import datetime
from tensorflow.keras.models import load_model
from utils.common import Common, Logger
from service.mongodb_service import MongodbService
from pathlib import Path
from utils.ml_utils import MlUtils


class PredictRNN(object):

    def __init__(self, logger, date):
        self.date = date
        self.config = Common.load_config('config/predict_RNN.yaml')['config']
        self.logger = logger
        self.database = self.config['database']
        self.mst_collection_name = self.config['mst_collection_name']
        self.mongo = MongodbService(self.logger)

    def create_input_table(self, device_id):
        data = self.mongo.find_all_message_in_3_days(collection_name=self.mst_collection_name,
                                                     device_id=device_id,)
        table_name = self.config['input_tbl_name'].format(device_id, self.date)

        if table_name in self.mongo.get_all_collection_names():
            self.logger.info('Collection %s exists', table_name)
            return

        else:
            collection = self.mongo.get_collection(database=self.database,
                                                   collection_name=table_name)
            self.mongo.insert_all(reversed(list(data)), collection)
            self.logger.info('Created table %s', table_name)

            return

    def predict_temperature(self, device_id):
        table_name = self.config['input_tbl_name'].format(device_id, self.date)
        data = self.mongo.find_per_1_minute_msg_1day(table_name, device_id)
        # max_timstamp = data[-1]
        # print(max_timstamp)
        p = Path(self.config['model_folder']).glob('*.h5')
        files = [f for f in p]

        for f in files:
            if f.stem.startswith('model{}'.format(device_id)):
                model_file = f
                break

        self.logger.info('Model file: %s', model_file)
        model = load_model(model_file)
        df = pd.DataFrame(list(data))
        temperature = df['temperature'].copy()
        temperature.index = df['timestamp']
        max_index = temperature.idxmax(axis=0)
        # print(max_index)
        ml_utils = MlUtils()
        timestep = self.config['timestep']
        Xs = ml_utils.transform_dataset(temperature, timestep)
        # print(Xs)
        Xs_shape = np.shape(Xs)
        predicts = model.predict(np.reshape(Xs, (Xs_shape[0], Xs_shape[1], 1)))
        # predict_pd = pd.DataFrame(predict)
        predicts_len = np.shape(predicts)[0]
        predict_idxs = []

        for i in range(predicts_len):
            predict_idxs.append(max_index + (60 * i))

        # print(predict_idxs)
        predicted_df = pd.DataFrame()
        # print(np.array(predicts).tolist())
        predicted_df['predicted_temperature'] = np.array(predicts.flatten()).tolist()
        predicted_df['timestamp'] = predict_idxs
        # predicted_df.index.name = 'timestamp'

        records = predicted_df.to_dict(orient='records')

        predicted_tbl_name = self.config['output_table_name']
        collection = self.mongo.get_collection('iot_platform',
                                               predicted_tbl_name.format(
                                                   device_id,
                                                   Common.get_datetime(self.date
                                                                       )))
        self.mongo.insert_all(records, collection)
        return

    def get_all_device_id(self):
        device_ids = []
        devices, _ = self.mongo.get_all('device')
        for device in devices:
            device_ids.append(device['device_id'])

        return device_ids

if __name__ == "__main__":
    logger = Logger(Common.get_file_name(__file__)).get_logger()
    from datetime import datetime
    predict = PredictRNN(logger, datetime.date(datetime.now()))
    # predict.create_input_table(2)
    predict.predict_temperature(2)
