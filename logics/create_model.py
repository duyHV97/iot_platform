import pandas as pd
import numpy as np
from pathlib import Path
from tensorflow.keras import Sequential, layers, optimizers
from tensorflow.keras.preprocessing.sequence import TimeseriesGenerator

from service.mongodb_service import MongodbService
from utils.common import Common, Logger
from utils.ml_utils import MlUtils


class CreateModel(object):

    def __init__(self, logger, date):
        self.date = date
        self.logger = logger
        self.config = Common.load_config('config/create_model.yaml')['config']
        self.mongo = MongodbService(log_object=self.logger)
        self.database = self.config['database']
        self.mst_collection = self.config['mst_collection']

    def create_data_table(self, device_id):
        collection_name = self.config['collection_name'].format(device_id,
                                                                Common.get_datetime(self.date))
        number_record = self.config['number_record']
        collection = self.mongo.get_collection(database=self.database,
                                               collection_name=collection_name)
        list_data = self.mongo.find_per_1_minute_msg_1day(collection_name=self.mst_collection,
                                                          device_id=device_id)
        try:
            self.mongo.insert_all(list_records=list_data, collection=collection)

            return

        except Exception as e:
            raise e

    def _create_1_feature_model(self, training_shape):
        model = Sequential()
        model.add(layer=layers.LSTM(units=128, input_shape=training_shape))
        model.add(layer=layers.Dense(units=1))
        model.compile(optimizer=optimizers.Adam(0.001),
                      loss='mse')

        return model

    def train_1_feature(self, device_id):
        self.logger.info('START create model for device_d: %s', device_id)
        collection_name = self.config['collection_name'].format(device_id,
                                                                Common.get_datetime(self.date))
        splitting_percent = self.config['splitting_percent']
        dataset, _ = self.mongo.get_all(collection_name=collection_name)
        dataset = pd.DataFrame(list(dataset))

        temperature_df = dataset[['temperature']].copy()
        temperature_df.index = dataset['timestamp']

        test_size = int(len(temperature_df) * splitting_percent)
        train_size = len(temperature_df) - test_size

        train, test = temperature_df.iloc[0: train_size], temperature_df.iloc[train_size:]

        ml_utils = MlUtils()
        time_steps = self.config['time_steps']

        # 10 minutes => 1
        X_train, y_train = ml_utils.create_timeseries_dataset(train,
                                                              train.temperature,
                                                              time_steps)
        X_test, y_test = ml_utils.create_timeseries_dataset(test,
                                                            test.temperature,
                                                            time_steps)

        training_shape = np.shape(X_train)
        model = self._create_1_feature_model(training_shape=(training_shape[1], training_shape[2]))

        epoch = self.config['epoch']
        batch_size = self.config['batch_size']
        history = model.fit(x=X_train,
                            y=y_train,
                            batch_size=batch_size,
                            epochs=epoch,
                            validation_split=0.1,
                            verbose=1,
                            shuffle=False)

        model_file_path = self.config['model_file_path']
        model.save(filepath=model_file_path.format(device_id, self.date))
        self.logger.info('END create model for device_d: %s', device_id)

    def get_all_device_id(self):
        device_ids = []
        devices, _ = self.mongo.get_all('device')
        for device in devices:
            device_ids.append(device['device_id'])

        return device_ids

    def drop_collections(self):
        collection_names = self.mongo.get_all_collection_names()

        for collection_name in collection_names:

            if collection_name.startswith('create_model_device'):
                self.mongo.drop_collection(collection_name=collection_name)


if __name__ == "__main__":
    logger = Logger(Common.get_file_name(__file__)).get_logger()
    from datetime import datetime
    create = CreateModel(logger, datetime.date(datetime.now()))
    # create.create_data_table(2)
    create.train_1_feature(2)
